/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author uSeR
 */
public class TestCustomerService {

    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0984571356"));
        
        //addNew
        Customer cusl = new Customer("tong", "0927670303");
        cs.addNew(cusl);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        
        Customer delCus = cs.getByTel("0927670303");
        delCus.setTel("0927670301");
        cs.update(delCus);
        System.out.println("After Updated");
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        
        cs.delete(delCus);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
    } 
}
