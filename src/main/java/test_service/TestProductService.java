/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.service.ProductService;

/**
 *
 * @author uSeR
 */
public class TestProductService {

    public static void main(String[] args) {
        ProductService ps = new ProductService();
        // getAll
        System.out.println("getAll");
        for (Product product : ps.getProducts()) {
            System.out.println(product);
        }
        // getById
        System.out.println("getById");
        System.out.println(ps.getById(1));

        // add new
//        Product r1 = new Product("Brownie", 30, "-", "-", "-", 2);
//        System.out.println("add new");
//        ps.addNew(r1);
//        for (Product product : ps.getProducts()) {
//            System.out.println(product);
//        }
        // update 
        Product delPro = ps.getById(5);
        delPro.setPrice(40);
        ps.update(delPro);
        System.out.println("After Updated");
        for (Product product : ps.getProducts()) {
            System.out.println(product);
        }
        
        // delete
        System.out.println("delete");
        delPro.setId(5);
        ps.delete(delPro);
        for(Product product : ps.getProducts()){
            System.out.println(product);
        }
    }
}
